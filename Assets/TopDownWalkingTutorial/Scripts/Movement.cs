﻿using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement; /**/
using UnityEngine.UI;/**/

public class Movement : MonoBehaviour
{
    public float MoveSpeed;
    public Slider Slider; /**/
    public Text Score; /**/

    private Vector3 moveVelocity;

    private Animator animator;
    private NavMeshAgent agent;

    // Use this for initialization
    void Start()
    {
        animator = this.GetComponent<Animator>();
        agent = this.GetComponent<NavMeshAgent>();

        Score.text = 0.ToString(); /**/
        Debug.Log("Start");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space)) animator.SetTrigger("Attack");

        if (agent.remainingDistance <= 0.2f)
        {
            animator.SetFloat("MoveSpeed", 0f);
        }

        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 1000f, LayerMask.GetMask("Ground")))
            {
                animator.SetFloat("MoveSpeed", 1f);
                agent.SetDestination(hit.point);
            }
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.name.Contains("LifePot"))
        {
            Slider.value += 0.2f;
            Destroy(col.gameObject);
        }
        else if(col.gameObject.name.Contains("PoisonPot"))
        {
            if (Slider.value - 0.4f <= 0)
            {
                SceneManager.LoadScene(0);
                return;
            }
            Slider.value -= 0.4f;

            Destroy(col.gameObject);
        }
        else if (col.gameObject.name.Contains("Coin"))
        {
            int currScore =  int.Parse(Score.text);
            currScore += 100;
            Score.text = currScore.ToString();

            Destroy(col.gameObject);
            Debug.Log("Coin");
        }
    }
}
